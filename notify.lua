local encoding = require 'encoding'
local fa = require 'fAwesome5'
local imgui_notf = require 'imgui'
u8 = encoding.UTF8
encoding.default = 'CP1251'

local ToScreen = convertGameScreenCoordsToWindowScreenCoords
local sX, sY = ToScreen(630, 438)
local message = {}
local msxMsg = 6
local notfList = {
	pos = {
		x = sX - 200, y = sY
	},
	npos = {
		x = sX - 200, y = sY
	},
	size = {
		x = 200, y = 0
	}
}

function style()
	imgui_notf.GetStyle().WindowRounding = 5.0
	imgui_notf.GetStyle().WindowPadding = imgui_notf.ImVec2(4.0, 4.0)
	imgui_notf.GetStyle().ChildWindowRounding = 5.0
	imgui_notf.GetStyle().Colors[imgui_notf.Col.Text] = imgui_notf.ImVec4(1.00, 1.00, 1.00, 1.00)
	imgui_notf.GetStyle().Colors[imgui_notf.Col.Border] = imgui_notf.ImVec4(0.0, 0.41, 0.76, 1.00)
	imgui_notf.GetStyle().Colors[imgui_notf.Col.WindowBg] = imgui_notf.ImVec4(0.04, 0.08, 0.10, 1.00)
end
style()

function main()
	while true do
		wait(0)
		imgui_notf.ShowCursor = false
		imgui_notf.SetMouseCursor(-1)
		imgui_notf.Process = true
	end
end


local fa_font = nil
local fontsize50 = nil
local fa_glyph_ranges = imgui_notf.ImGlyphRanges({ fa.min_range, fa.max_range })
function imgui_notf.BeforeDrawFrame()
    if fa_font == nil then
        local font_config = imgui_notf.ImFontConfig()
        font_config.MergeMode = true

        fa_font = imgui_notf.GetIO().Fonts:AddFontFromFileTTF('moonloader/GOVh/fa-solid-900.ttf', 50.0, font_config, fa_glyph_ranges)
    end
    if fontsize50 == nil then
        fontsize50 = imgui_notf.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\trebucbd.ttf', 70.0, nil, imgui_notf.GetIO().Fonts:GetGlyphRangesCyrillic())
    end
end


function imgui_notf.OnDrawFrame()
	local count = 0
	for k, v in ipairs(message) do
		local push = false
		if v.active and v.time < os.clock() then
			v.active = false
		end
		if count < msxMsg then
			if not v.active then
				if v.showtime > 0 then
					v.active = true
					v.time = os.clock() + v.showtime
					v.showtime = 0
				end
			end
			if v.active then
				count = count + 1
				if v.time + 3.000 >= os.clock() then
					imgui_notf.PushStyleVar(imgui_notf.StyleVar.Alpha, (v.time - os.clock()) / 1.0)
					push = true
				end
				local nText = u8(tostring(v.text))
				notfList.pos = imgui_notf.ImVec2(notfList.pos.x, notfList.pos.y - 75)

				imgui_notf.SetNextWindowPos(notfList.pos, _, imgui_notf.ImVec2(0.0, 0.25))
				imgui_notf.SetNextWindowSize(imgui_notf.ImVec2(250, 65))
				imgui_notf.Begin(u8'##msg'..k, _, imgui_notf.WindowFlags.NoCollapse + imgui_notf.WindowFlags.NoResize + imgui_notf.WindowFlags.NoScrollbar + imgui_notf.WindowFlags.NoMove + imgui_notf.WindowFlags.NoTitleBar)
				-->
					imgui_notf.BeginChild('##govLogo'..k, imgui_notf.ImVec2(70, -1), false, imgui_notf.WindowFlags.NoScrollbar)
						imgui_notf.SetCursorPos(imgui_notf.ImVec2(10, 35))
						imgui_notf.TextColored(imgui_notf.ImVec4(0.0, 0.41, 0.76, 1.00), fa.ICON_FA_UNIVERSITY)
						imgui_notf.SetCursorPos(imgui_notf.ImVec2(52.5, -12))
						imgui_notf.PushFont(fontsize50)
						imgui_notf.TextColored(imgui_notf.ImVec4(0.0, 0.41, 0.76, 1.00), '|')
						imgui_notf.PopFont()
					imgui_notf.EndChild()

						imgui_notf.SameLine()

					imgui_notf.BeginChild('##govText'..k, imgui_notf.ImVec2(-1, -1), false, imgui_notf.WindowFlags.NoScrollbar)
					    imgui_notf.SetCursorPosY(2.5)
					    imgui_notf.TextColoredRGB('{006AC2}GOV-Helper\n'..u8:decode(nText))
					imgui_notf.EndChild()
				--<
				imgui_notf.End()
				if push then
					imgui_notf.PopStyleVar()
				end
			end
		end
	end
	sX, sY = ToScreen(605, 438)
	notfList = {
		pos = {
			x = sX - 200,
			y = sY + 20
		},
		npos = {
			x = sX - 200,
			y = sY
		},
		size = {
			x = 200,
			y = 0
		}
	}
end

function EXPORTS.addNotify(text, time)
	message[#message + 1] = { active = false, time = 0, showtime = time, text = text }
end

function imgui_notf.TextColoredRGB(text)
    local style = imgui_notf.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui_notf.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui_notf.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui_notf.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui_notf.SameLine(nil, 0)
                end
                imgui_notf.NewLine()
            else imgui_notf.Text(u8(w)) end
        end
    end

    render_text(text)
end
