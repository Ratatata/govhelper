script_name('GOV-Helper')
script_version('20')
local scriptversion = '1.4.5 Beta'
script_description('06.02.2021')

require 'lib.moonloader'
local dlstatus = require 'moonloader'.download_status
local as = require('moonloader').audiostream_state
local bSampev, sampev 		= pcall(require, 'lib.samp.events')
local bInicfg, inicfg 		= pcall(require, 'inicfg')
local bImgui, imgui 		= pcall(require, 'imgui')
local bVkeys, vkeys 		= pcall(require, 'vkeys')
local bNotf, notify			= pcall(import, getWorkingDirectory()..'\\GOVh\\notify')
local bFa, fa 				= pcall(require, 'fAwesome5')
local bEncoding, encoding 	= pcall(require, 'encoding')

encoding.default = 'CP1251'
u8 = encoding.UTF8
changelog = {
    [5] = {
        ['date'] = '12.02.2021',
        ['ver'] = '1.4.5 Beta',
        ['info'] = {
            '������ ������ ������ ���. ����� �� 5, 15, 25 � �.�. ������',
            '� ���� �������������� ������ ����� �������� ���� ���������� (BETA)'
        },
        ['patches'] = {
            ['show'] = false,
            ['info'] = {
            }
        }
    },
    [4] = {
        ['date'] = '09.02.2021',
        ['ver'] = '1.4.0 Stable',
        ['info'] = {
            '��������� ���� �������������� � ����������� (���������/����������). ��� ��������� ������� ��� + R �� ����������',
        },
        ['patches'] = {
            ['show'] = false,
            ['info'] = {
            }
        }
    },
    [3] = {
        ['date'] = '06.02.2021',
        ['ver'] = '1.3.0 Stable',
        ['info'] = {
            '��������� ���� ��������� � ���������� (/clog)',
        },
        ['patches'] = {
            ['show'] = false,
            ['info'] = {
                '���������� ������ ������',
            }
        }
    },
    [2] = {
        ['date'] = '05.02.2021',
        ['ver'] = '1.2.0 Stable',
        ['info'] = {
            '��������� ������������ ������� /giverank',
            '��������� ��������� �������� ������',
            '��������� ������� ����� ��� ������������'
        },
        ['patches'] = {
            ['show'] = false,
            ['info'] = {
                '�������������� ��������',
            }
        }
    },

    [1] = {
        ['date'] = '04.02.2021',
        ['ver'] = '1.1.0 Stable',
        ['info'] = {
            '��������� �������� ���������� ����� ������� ���. �����',
        },
        ['patches'] = {
            ['show'] = false,
            ['info'] = {}
        }
    },

    [0] = {
        ['date'] = '03.02.2021',
        ['ver'] = '1.0.0 Release',
        ['info'] = {
            '����� �������',
        },
        ['patches'] = {
            ['show'] = false,
            ['info'] = {
                '���������� ������ ������',
            }
        }
    }

}
local configuration = inicfg.load({
    main = {
        gosscreen = true,
        gossound = true,
        delaygov = 2500,
        clogshow = false
    },
    sound = {
        volume = 100,
        url = 'https://www.myinstants.com/media/sounds/201159__kiddpark__cash-register.mp3'
    },
	govstr = {
		'[�������������] ��������� ������ �����!',
		'[�������������] �� ������ ������ �������� ���� �������� ������ � �����!',
		'[�������������] ��� ����� ������� � ���� �����! ��� ���!'
	},
	govdep = {
		'[���-��] - [����] ������� ���.�����, ������� �� ����������',
		'[���-��] - [����] ���������� ���.�����'
    },
    time = {
        hour = 15,
        minute = 30
    },
    logs = {
        ['/giverank'] = '!date !time | !myName ������� ���� !member � !rankNameOld(!rankOld) �� !rankNameNew(!rankNew)',
    },
    ranks = {
        [1]  = '��������',
		[2]  = '��������',
		[3]  = '����� ��',
		[4]  = '��. ����� ��',
		[5]  = '���. �������',
		[6]  = '��������',
		[7]  = '���� ���. ��������',
		[8]  = '������������ �����',
		[9]  = '����-����������',
		[10] = '����������'
    }
}, 'GOVconfig')
mainPath = getWorkingDirectory().."\\GOVh\\Log"
ImEdit = {
	['/giverank'] 	= imgui.ImBuffer(u8(configuration.logs['/giverank']), 256)
}
ImRank 	= {
	[1]  = imgui.ImBuffer(u8(configuration.ranks[1]), 256),
	[2]  = imgui.ImBuffer(u8(configuration.ranks[2]), 256),
	[3]  = imgui.ImBuffer(u8(configuration.ranks[3]), 256),
	[4]  = imgui.ImBuffer(u8(configuration.ranks[4]), 256),
	[5]  = imgui.ImBuffer(u8(configuration.ranks[5]), 256),
	[6]  = imgui.ImBuffer(u8(configuration.ranks[6]), 256),
	[7]  = imgui.ImBuffer(u8(configuration.ranks[7]), 256),
	[8]  = imgui.ImBuffer(u8(configuration.ranks[8]), 256),
	[9]  = imgui.ImBuffer(u8(configuration.ranks[9]), 256),
	[10] = imgui.ImBuffer(u8(configuration.ranks[10]), 256)
}
local ImLogs = imgui.ImBuffer(131072)
local mc, sc, wc = '{006AC2}', '{006D86}', '{FFFFFF}'
local mcx = 0x006AC2
local mGov 				= imgui.ImInt(configuration.time.minute)
local hGov 				= imgui.ImInt(configuration.time.hour)
local status_button_gov = false
local gosScreen 		= imgui.ImBool(configuration.main.gosscreen)
local gosSound 			= imgui.ImBool(configuration.main.gossound)
--local inafk = false
local sound = {
    ['url'] = imgui.ImBuffer(tostring(configuration.sound.url), 256),
    ['vol'] = imgui.ImInt(configuration.sound.volume)
}
local delayGov 			= imgui.ImInt(configuration.main.delaygov)
local govstr = {
	[1] = imgui.ImBuffer(u8(configuration.govstr[1]), 256),
	[2] = imgui.ImBuffer(u8(configuration.govstr[2]), 256),
	[3] = imgui.ImBuffer(u8(configuration.govstr[3]), 256)
}
local govdep = { 
	[1] = imgui.ImBuffer(u8(configuration.govdep[1]), 256),
	[2] = imgui.ImBuffer(u8(configuration.govdep[2]), 256),
}
local jsn_upd = "https://gitlab.com/Ratatata/govhelper/-/snippets/2070000/raw/master/GOV-Helper-version.json"
local tag = '[ GOVHelper ] '..wc


local imguiWindows = {
    {'main', true, false},
    {'log', true, false},
    {'changelog', true, false},
    {'vz', true, false}
}
for k, v in pairs(imguiWindows) do
    _G['IW_'..v[1]] = imgui.ImBool(false)
end

local actionId = ''
local TypeAction = imgui.ImInt(1)
local giverank = imgui.ImInt(2)
local uvalreason = imgui.ImBuffer(64)
local chsreason = imgui.ImBuffer(64)
local getMembers = false
local countt = 0
local online = 0
local namerank = '�� ���������'
local members = {}
function main()
	if not isSampLoaded() or not isSampfuncsLoaded() then return end
    while not isSampAvailable() do wait(100) end
        sampRegisterChatCommand('govh', function() IW_main.v = not IW_main.v end)
        sampRegisterChatCommand('log', function() IW_log.v = not IW_log.v end)
        sampRegisterChatCommand('clog', function() IW_changelog.v = not IW_changelog.v end)
        sampRegisterChatCommand('vz', vzzz)
        log('���������� � �������..')

        --checkupdate(jsn_upd, tag, url_upd)

        dData = checkData() 
        if not doesFileExist('moonloader/config/GOVconfig.ini') then
            if inicfg.save(configuration, 'GOVconfig.ini') then log('������ ���� GOVconfig.ini') end
        end
        while not dData do wait(0) end
        loadlogs()
        log('�����!')
        if loadSounds() then log('���� {006AC2}��������') end
        while not sampIsLocalPlayerSpawned() do wait(0) end
        wait(2000)
        notify.addNotify('������ ��������!\n������� ����: {006AC2}/govh', 5)
        if configuration.main.clogshow then IW_changelog.v = true end
        --if sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))):find('son') then lua_thread.create(timerupdate) end
        --if sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))):find('son') then lua_thread.create(checkMembers) end
        if sampGetPlayerNickname(select(2, sampGetPlayerIdByCharHandle(playerPed))) ~= 'Ethan_Anderson' then 
            sampAddChatMessage('������', -1)
            thisScript():unload() 
        end
        lua_thread.create(timerupdate)
        lua_thread.create(checkMembers)
        getMembers = true
        sampSendChat('/members')
	while true do
        local selfId = select(2, sampGetPlayerIdByCharHandle(playerPed))
    	tInfo = {
    		['selfId'] = selfId,
			['selfName'] = sampGetPlayerNickname(selfId),
			['date'] = os.date("%d.%m.%Y", os.time()),
			['time'] = os.date("%H:%M:%S", os.time())
    	}
        wait(0)
        local imguiStatus = {false, false, false}
        for k, v in pairs(imguiWindows) do
            if _G['IW_'..v[1]].v then
                imguiStatus[1] = true
                if v[2] then imguiStatus[2] = true end
                if v[3] then imguiStatus[3] = true end
            end
        end
        imgui.Process = imguiStatus[1]
        imgui.ShowCursor = imguiStatus[2]
        imgui.LockPlayer = imguiStatus[3]
        if status_button_gov then 
            if tonumber(os.date("%S", os.time())) == 00 and antiflud then
	        	if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v - 3 then
        			antiflud = false
                    notify.addNotify("����� {006AC2}3 ������{SSSSSS} GOV �����\n{006D86}����������� � ����!", 10)
                    if gosSound.v and soundurl then setAudioStreamState(soundurl, as.PLAY) end
	        	end
	        end
        	if tonumber(os.date("%S", os.time())) == 00 and antiflud then
	        	if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v - 1 then
        			antiflud = false
                    notify.addNotify("����� {006AC2}������{SSSSSS} GOV �����\n{006D86}����������� � ����!", 10)
                    if gosSound.v and soundurl then setAudioStreamState(soundurl, as.PLAY) end
	        	end
	        end
        	if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v then 
        		status_button_gov = false
                goGov()
        	end
        end
        if isKeyJustPressed(82) then
	        local validtar, pedtar = getCharPlayerIsTargeting(playerHandle)
	        if validtar and doesCharExist(pedtar) then
	            local result, id = sampGetPlayerIdByCharHandle(pedtar)
                actionId = id
	            if result then
                    if #members > 0 then
                        --[[for key, box in pairs(members) do
                            if tonumber(key) <= tonumber(online) then
                                if tonumber(box[2]) == id then
                                    namerank = tostring(box[3])
                                end
                            end
                        end]]
                        for k, v in pairs(members) do
                            if tonumber(key) <= tonumber(online) then
                                if tonumber(v[1]) == tonumber(actionId) then
                                    namerank = tostring(v[2])
                                end
                            end
                        end
                    else
                        namerank = '�� ���������'
                    end
                    IW_vz.v = true 
                end
	        end
    	end
	end
end
function loadSounds()
    soundurl    = loadAudioStream(sound['url'].v)
    --
    if soundurl then setAudioStreamVolume(soundurl, sound['vol'].v) end
    return true
end
local fa_font = nil
local fs18, fs20, fs35 = nil, nil, nil
local fa_glyph_ranges = imgui.ImGlyphRanges({ fa.min_range, fa.max_range })
function imgui.BeforeDrawFrame()
    if fa_font == nil then
        local font_config = imgui.ImFontConfig()
        font_config.MergeMode = true

        fa_font = imgui.GetIO().Fonts:AddFontFromFileTTF('moonloader/GOVh/fa-solid-900.ttf', 13.0, font_config, fa_glyph_ranges)
    end
    if fs35 == nil then fs35 = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\trebucbd.ttf', 35.0, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic()) end
    if fs18 == nil then fs18 = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\trebucbd.ttf', 18.0, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic()) end
    if fs20 == nil then fs20 = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\trebucbd.ttf', 20.0, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic()) end
end
function imgui.OnDrawFrame()
    local sw, sh = getScreenResolution()
    if IW_vz.v then
        imgui.SetNextWindowPos(imgui.ImVec2(sw / 2, sh / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.SetNextWindowSize(imgui.ImVec2(465, 223), imgui.Cond.FirstUseEver)
        imgui.Begin(u8'���� ��������������', _, imgui.WindowFlags.NoResize)
        imgui.BeginChild("##ActionIdInfo", imgui.ImVec2(150, 108), true, imgui.WindowFlags.NoScrollbar) --���� 180
	        imgui.CenterTextColoredRGB(mc..'��� ����������')
	        if sampIsPlayerConnected(actionId) then
	        	imgui.CenterTextColoredRGB(rpNick(actionId)..'['..actionId..']')
	        	if imgui.IsItemClicked() then
		            if setClipboardText(u8:decode((sampGetPlayerNickname(actionId)))) then
		            	sampAddChatMessage(tag..'��� '..mc..rpNick(actionId)..wc..' ���������� � ������ ������!', mcx)
		            end
		        else imgui.Hint(u8'�����, ���-�� �����������') end
                imgui.NewLine()
                imgui.CenterTextColoredRGB(mc..'���������')
                --imgui.CenterTextColoredRGB('�������� (1)')
                imgui.CenterTextColoredRGB(namerank)
		    else
                IW_vz.v = false
	        	sampAddChatMessage(tag..'����� ����� �� ����!', mcx)
	        end
        imgui.EndChild()
        imgui.SameLine()
        if TypeAction.v == 0 then
            imgui.BeginChild("##Actions0", imgui.ImVec2(-1, 192), true, imgui.WindowFlags.NoScrollbar)
                imgui.CenterTextColoredRGB(mc..'�������� ���� ������� ������ ������')
                imgui.NewLine()
                imgui.SetCursorPosX(33)
                imgui.TextDisabled('1'); imgui.SameLine(60)
                imgui.TextDisabled('2'); imgui.SameLine(88)
                imgui.TextDisabled('3'); imgui.SameLine(115)
                imgui.TextDisabled('4'); imgui.SameLine(142)
                imgui.TextDisabled('5'); imgui.SameLine(168)
                imgui.TextDisabled('6'); imgui.SameLine(196)
                imgui.TextDisabled('7'); imgui.SameLine(223)
                imgui.TextDisabled('8'); imgui.SameLine(249)
                imgui.TextDisabled('9')

                imgui.SetCursorPosX(28)
                imgui.RadioButton(u8("##giverank1"), giverank, 1); imgui.Hint(u8(configuration.ranks[1])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank2"), giverank, 2); imgui.Hint(u8(configuration.ranks[2])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank3"), giverank, 3); imgui.Hint(u8(configuration.ranks[3])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank4"), giverank, 4); imgui.Hint(u8(configuration.ranks[4])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank5"), giverank, 5); imgui.Hint(u8(configuration.ranks[5])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank6"), giverank, 6); imgui.Hint(u8(configuration.ranks[6])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank7"), giverank, 7); imgui.Hint(u8(configuration.ranks[7])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank8"), giverank, 8); imgui.Hint(u8(configuration.ranks[8])); imgui.SameLine()
                imgui.RadioButton(u8("##giverank9"), giverank, 9); imgui.Hint(u8(configuration.ranks[9]));
                imgui.NewLine()
                imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.00, 0.43, 0.66, 1.00))
                imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.00, 0.33, 0.56, 1.00))
                imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.00, 0.23, 0.46, 1.00))
                if imgui.Button(u8('�������� ')..fa.ICON_FA_SORT_NUMERIC_UP, imgui.ImVec2(-1, 45)) then
                    IW_vz.v = false
                    lua_thread.create(function()
                        sampSendChat('/me ������ �� ������� ���')
                        wait(2000)
                        sampSendChat('/me ������� ��� � ����� � ������ "����������"')
                        wait(2000)
                        sampSendChat('/me ������ ���������� '..rpNick(actionId))
                        wait(2000)
                        sampSendChat('/me ������� ��������� ���������� �� '..configuration.ranks[giverank.v])
                        wait(2000)
                        sampSendChat('/giverank '..actionId..' '..giverank.v)
                        sampSendChat('��������� ���!')
                    end)
                end
                imgui.PopStyleColor(3)
                if imgui.Button(u8('�������� ')..fa.ICON_FA_SORT_NUMERIC_DOWN, imgui.ImVec2(-1, -1)) then
                    IW_vz.v = false
                    lua_thread.create(function()
                        sampSendChat('/me ������ �� ������� ���')
                        wait(2500)
                        sampSendChat('/me ������� ��� � ����� � ������ "����������"')
                        wait(2500)
                        sampSendChat('/me ������ ���������� '..rpNick(actionId))
                        wait(2500)
                        sampSendChat('/me ������� ��������� ���������� �� '..configuration.ranks[giverank.v])
                        wait(2500)
                        sampSendChat('/giverank '..actionId..' '..giverank.v)
                    end)
                end
            imgui.EndChild()
        end
        if TypeAction.v == 1 then
            imgui.BeginChild("##Actions1", imgui.ImVec2(-1, 187), true, imgui.WindowFlags.NoScrollbar)
                imgui.CenterTextColoredRGB('������� '..mc..'�������{SSSSSS} ���������� � ��')
                imgui.NewLine()
                imgui.NewLine()
                imgui.PushItemWidth(-1)
                imgui.InputText('##uvalreason', uvalreason)
                imgui.VoidText(uvalreason.v, '������� ����������')
                imgui.NewLine()
                imgui.InputText('##chsreason', chsreason)
                imgui.VoidText(chsreason.v, '������� �� (��� �������������)')
                if #uvalreason.v > 0 then
                    imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.00, 0.43, 0.66, 1.00))
                    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.00, 0.33, 0.56, 1.00))
                    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.00, 0.23, 0.46, 1.00))
                    if imgui.Button(u8('������� ')..fa.ICON_FA_MINUS_CIRCLE, imgui.ImVec2(-1, -1)) then
                        IW_vz.v = false
                        TypeAction.v = 0
                        lua_thread.create(function()
                            sampSendChat('/me ������ �� ������� ��� � ������� ���')
                            wait(2000)
                            sampSendChat('/me ������ ������ �����������')
                            wait(2000)
                            sampSendChat('/me ������ ���� ���������� '..rpNick(tonumber(actionId.v)))
                            wait(2000)
                            sampSendChat('/me ������ ���������� �� �����������')
                            wait(500)
                            sampSendChat('/uninvite '..actionId.v..' '..tostring(u8:decode(uvalreason.v)))
                            if #chsreason.v > 0 then
                                wait(2000)
                                sampSendChat('/me �������� ������ �׸���� ������')
                                wait(2000)
                                sampSendChat('/me ���� '..rpNick(tonumber(actionId.v))..' � ������ ������ �����������')
                                wait(500)
                                sampSendChat('/blacklist '..actionId.v..' '..tostring(u8:decode(chsreason.v)))
                            end
                            uvalreason.v, chsreason.v = '', ''
                        end)
                    end
                    imgui.PopStyleColor(3)
                else
                    imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.20, 0.25, 0.29, 1.00))
                    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.20, 0.25, 0.29, 1.00))
                    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.20, 0.25, 0.29, 1.00))
                    imgui.Button(u8('������� ')..fa.ICON_FA_MINUS_CIRCLE, imgui.ImVec2(-1, -1))
                    imgui.PopStyleColor(3)
                end
            imgui.EndChild()
        end
        imgui.SetCursorPos(imgui.ImVec2(10, 153))
        imgui.RadioButton(u8'���������', TypeAction, 0)
        imgui.SetCursorPos(imgui.ImVec2(10, 183))
        imgui.RadioButton(u8'����������', TypeAction, 1)
        imgui.End()
    end
    if IW_changelog.v then
        imgui.SetNextWindowPos(imgui.ImVec2(sw / 2, sh / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.SetNextWindowSize(imgui.ImVec2(sw / 1.5, sh / 1.5), imgui.Cond.FirstUseEver)

        imgui.Begin('##ChangeLogWindow', _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.ShowBorders)
            imgui.PushFont(fs35)
            imgui.CenterTextColoredRGB(mc..'������ ���������')
            imgui.PopFont()
            imgui.CenterTextColoredRGB('{SSSSSS}������: '..mc..scriptversion..'{SSSSSS} | ��������� ���������� ����� '..mc..updateDate())
            imgui.PushStyleColor(imgui.Col.ChildWindowBg, imgui.ImVec4(0.07, 0.07, 0.07, 1.00))
            imgui.BeginChild('##ChangeLog', imgui.ImVec2(-1, imgui.GetWindowHeight() - 105), true)
                for count = 0, #changelog do
                    local count = #changelog - count
                    local ver = changelog[count]['ver']
                    local date = changelog[count]['date']
                    local info = changelog[count]['info']
                    local patches = changelog[count]['patches']['info']

                    imgui.PushFont(fs20)
                    imgui.TextColoredRGB(u8(wc..ver))
                    imgui.PopFont()
                    --imgui.SameLine()

                    imgui.PushTextWrapPos(imgui.GetWindowWidth() - 30)
                    for k, v in ipairs(info) do
                        imgui.TextColoredRGB(u8('{606060}'..k..') {SSSSSS}'..v))
                    end
                    if #info <= 0 then imgui.TextColoredRGB(u8('{505050}(������ ������ �� ����� ���������)')) end

                    if #patches > 0 then
                        imgui.TextColoredRGB(u8(mc..'>> ������ ������ ('..#patches..')'))
                        imgui.Hint(u8('��������, ��� �� '..(changelog[count]['patches']['show'] and '��������' or '����������')))
                        if imgui.IsItemClicked() then 
                            changelog[count]['patches']['show'] = not changelog[count]['patches']['show']
                        end
                        if changelog[count]['patches']['show'] then
                            local p1 = imgui.GetCursorScreenPos()
                            for _, info in ipairs(patches) do
                                imgui.SetCursorPosX(40)
                                imgui.TextColoredRGB(u8('{909090}'..info))
                            end
                            local p2 = imgui.GetCursorScreenPos()
                            imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p1.x + 20.5, p1.y + 2), imgui.ImVec2(p2.x + 20, p2.y - 2), 0xFF00C235, 2)
                        end
                    end
                    imgui.PopTextWrapPos()

                    imgui.NewLine()
                end
            imgui.EndChild()
            imgui.SetCursorPosX((imgui.GetWindowWidth() - 300) / 2)
            
            --imgui.PushStyleColor(imgui.Col.Border, imgui.ImVec4(0, 1, 0, 1))
            imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(imgui.GetStyle().Colors[imgui.Col.WindowBg]))
            if imgui.Button(u8'������� ����', imgui.ImVec2(300, 30)) then 
                configuration.main.clogshow = false
                inicfg.save(configuration, 'GOVconfig.ini')
                IW_changelog.v = false 
            end
            imgui.PopStyleColor(2)
        imgui.End()
    end
    if IW_log.v then
        imgui.SetNextWindowPos(imgui.ImVec2(sw / 2, sh / 2), imgui.Cond.Always, imgui.ImVec2(0.5, 0.5))
        imgui.SetNextWindowSize(imgui.ImVec2(650, 300), imgui.Cond.FirstUseEver)
        imgui.Begin(u8'������������ ��������', _, imgui.WindowFlags.NoResize + imgui.WindowFlags.AlwaysAutoResize)
            imgui.PushFont(fs20)
		   	imgui.CenterTextColoredRGB('������������ �������� '..mc..'/giverank')
		    imgui.PopFont()
            local pathSelect = mainPath..'\\giverank.txt'
		    imgui.CenterTextColoredRGB('{606060}'..pathSelect)
            if imgui.IsItemHovered() and imgui.IsMouseDoubleClicked(0) then os.execute('explorer '..pathSelect) end
		    imgui.Hint(u8'������ �������� ���-�� ������� ���� ��������� ����')
            imgui.SetCursorPosX((imgui.GetWindowWidth() - 200) / 2)
	    	if imgui.Button(u8'�������� �������� ������', imgui.ImVec2(200, 20)) then 
	    		imgui.OpenPopup('##ranknames')
	    	end
            if imgui.BeginPopupModal('##ranknames', _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.AlwaysAutoResize) then
                imgui.PushFont(fs18)
                imgui.CenterTextColoredRGB(mc..'��������� �������� ������')
                imgui.PopFont()
                imgui.NewLine()
                for k = 1, 10 do 
                    imgui.PushItemWidth(300)
                    imgui.Text(u8(k..' ����')); imgui.SameLine(60)
                    imgui.InputText('##editrank'..k, ImRank[k])
                    imgui.SameLine()
                    if ImRank[k].v ~= u8(configuration.ranks[k]) and #ImRank[k].v > 0 then 
                        if imgui.Button(u8'���������##'..k, imgui.ImVec2(100, 20)) then 
                            configuration.ranks[k] = u8:decode(ImRank[k].v)
                            inicfg.save(configuration, 'GOVconfig.ini')
                        end
                    else
                        imgui.DisableButton(u8'���������##'..k, imgui.ImVec2(100, 20))
                    end
	    		    imgui.PopItemWidth()
	    	    end
                imgui.SetCursorPosX((imgui.GetWindowWidth() - 200) / 2)
                if imgui.Button(u8'�������', imgui.ImVec2(200, 25)) then imgui.CloseCurrentPopup() end
            imgui.EndPopup()
            end
            imgui.TextColoredRGB(u8(mc..'����� ������������ c �������������� {SSSSSS}�����:'))
            if ImEdit['/giverank'].v ~= u8(configuration.logs['/giverank']) then 
		    	imgui.SameLine()
		    	if imgui.SmallButton(u8'���������') then 
		    		configuration.logs['/giverank'] = u8:decode(ImEdit['/giverank'].v)
		    		inicfg.save(configuration, 'GOVconfig.ini')
		    	end
		    end
            imgui.PushItemWidth(650)
		    imgui.InputText('##EditForma', ImEdit['/giverank'])
            imgui.SameLine()
            if imgui.Button(u8'����', imgui.ImVec2(100, 20)) then 
		    	imgui.OpenPopup('##allPatterns')
		    end
            if imgui.BeginPopupContextItem('##allPatterns') then
                imgui.PushFont(fs18)
                   imgui.CenterTextColoredRGB(mc..'�������������� ����')
                imgui.PopFont()
                   imgui.CenterTextColoredRGB('{606060}������� ����� ��������')
                imgui.NewLine()
                showPattern('!member', 		'�������� Nick_Name ������') 
                showPattern('!rankNameOld', '�������� ������ �������� �����')
                showPattern('!rankNameNew', '�������� ����� �������� �����')
                showPattern('!rankOld', 	'�������� ����� ������� �����')
                showPattern('!rankNew', 	'�������� ����� ������ �����')
                showPattern('!date', 		'�������� ������� ����') 
                showPattern('!time', 		'�������� ������� �����') 
                showPattern('!myName', 		'�������� ��� Nick_Name') 
                showPattern('!myId', 		'�������� ��� ID') 
                imgui.EndPopup()
            end
            imgui.TextColoredRGB(u8(mc..'������� �����: {606060}(������)'))
		    imgui.TextColoredRGB(u8(getExitString('/giverank', true)))
		    imgui.PopItemWidth()
		    imgui.NewLine()
            local getLog = io.open(mainPath..'\\giverank.txt', 'r+')
            local tLogTxt = {}
            for str in getLog:lines() do
                table.insert(tLogTxt, str)
            end getLog:close()
            ImLogs.v = u8(table.concat(tLogTxt, '\n'))
            imgui.InputTextMultiline("##LogList", ImLogs, imgui.ImVec2(758, 400), imgui.InputTextFlags.ReadOnly)
		    if #ImLogs.v == 0 then
			    imgui.SetCursorPosY(380)
			    imgui.CenterTextColoredRGB('{606060}�����')
			end
        imgui.End()
    end
    if IW_main.v then
        imgui.SetNextWindowPos(imgui.ImVec2(sw / 2, sh / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.SetNextWindowSize(imgui.ImVec2(648, 272), imgui.Cond.FirstUseEver)
        imgui.Begin(u8'������������ ��������������� �����', _, imgui.WindowFlags.AlwaysAutoResize + imgui.WindowFlags.NoResize)
            if hGov.v > 23 then hGov.v = 0 end
            if mGov.v > 59 then mGov.v = 0 end
            if hGov.v < 0 then hGov.v = 23 end
            if mGov.v < 0 then mGov.v = 55 end

            imgui.PushStyleVar(imgui.StyleVar.ItemSpacing, imgui.ImVec2(1, 1))
            imgui.SetCursorPosX(30)
            if imgui.Button('+##PlusHour', imgui.ImVec2(40, 20)) then
                status_button_gov = false
                hGov.v = hGov.v + 1
            end
            imgui.SameLine(93)
            if imgui.Button('+##PlusMin', imgui.ImVec2(40, 20)) then
                status_button_gov = false 
                mGov.v = mGov.v + 10
            end

            imgui.PushFont(fs35)
            if #tostring(hGov.v) == 1 then zeroh = '0' else zeroh = '' end
            if #tostring(mGov.v) == 1 then zerom = '0' else zerom = '' end
            imgui.SetCursorPosX(30)
            imgui.TextColoredRGB(tostring(zeroh..hGov.v)..' : '..tostring(zerom..mGov.v))
            imgui.PopFont()

            imgui.SetCursorPosX(30)
            if imgui.Button('-##MinusHour', imgui.ImVec2(40, 20)) then
                status_button_gov = false
                hGov.v = hGov.v - 1
            end
            imgui.SameLine(93)
            if imgui.Button('-##MinusMin', imgui.ImVec2(40, 20)) then
                status_button_gov = false
                mGov.v = mGov.v - 10
            end
            imgui.PopStyleVar()

            imgui.CenterTextColoredRGB(mc..'����� ��������������� �����:')
            imgui.PushItemWidth(600)
            imgui.TextDisabled('/d'); imgui.SameLine(40); imgui.InputText('##govdep1', govdep[1])
            imgui.TextDisabled('/gov'); imgui.SameLine(40); imgui.InputText('##govstr1', govstr[1])
            imgui.TextDisabled('/gov'); imgui.SameLine(40); imgui.InputText('##govstr2', govstr[2])
            imgui.TextDisabled('/gov'); imgui.SameLine(40); imgui.InputText('##govstr3', govstr[3])
            imgui.TextDisabled('/d'); imgui.SameLine(40); imgui.InputText('##govdep2', govdep[2])
            imgui.PopItemWidth()
            imgui.SetCursorPosX((imgui.GetWindowWidth() - 160) / 2)
            if imgui.Button(u8'���������', imgui.ImVec2(160, 20)) then
                configuration.main.delaygov = delayGov.v
                configuration.main.gossound = gosSound.v
                configuration.main.gosscreen = gosScreen.v
                configuration.govstr[1] = u8:decode(govstr[1].v)
                configuration.govstr[2] = u8:decode(govstr[2].v)
                configuration.govstr[3] = u8:decode(govstr[3].v)
                --
                configuration.govdep[1] = u8:decode(govdep[1].v)
                configuration.govdep[2] = u8:decode(govdep[2].v)
                if inicfg.save(configuration, 'GOVconfig.ini') then
                    notify.addNotify('��������� ���������!', 5)
                    IW_main.v = false
                else 
                    notify.addNotify('��������� {006AC2}������{SSSSSS}!\n��������� {006AC2}�� ���������{SSSSSS}!', 5)
                    IW_main.v = false
                end
            end
            imgui.SetCursorPos(imgui.ImVec2(150, 30))
            imgui.BeginChild("##SettingsGov", imgui.ImVec2(-1, 75), true)
            imgui.ToggleButton('##gosScreen', gosScreen); imgui.SameLine()
            imgui.TextColoredRGB(u8('����-����� ����� ������'))
            imgui.ToggleButton('##gosSound', gosSound); imgui.SameLine()
            imgui.TextColoredRGB(u8('����������� �����'))
            --imgui.Question(u8'�� ������ �� ������ GOV-����� ����� ����������� ����')
            if gosSound.v then
                imgui.SameLine()
                imgui.TextColoredRGB('{5C6B78}'..fa.ICON_FA_COG)
                if imgui.IsItemClicked() then
                    imgui.OpenPopup(u8'��������� ������')
                end
                if imgui.IsItemHovered() then
                    imgui.Hint(u8'�������, ����� ������� ��������� �������� ����������')
                end
                if imgui.BeginPopupModal(u8'��������� ������', _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.AlwaysAutoResize) then
                    imgui.CenterTextColoredRGB(mc..'�� ������ �� ������ GOV-����� ����� ���������������� ����')
                    imgui.NewLine()
                    imgui.PushItemWidth(100) 
                    if imgui.SliderInt(u8('���������'), sound['vol'], 1, 100, "%.0f%%") then 
                        configuration.sound.volume = sound['vol'].v
                        setAudioStreamVolume(soundurl, sound['vol'].v)
                    end
                    imgui.PopItemWidth()
                    imgui.PushItemWidth(389) 
        
                    imgui.InputText('##Sound', sound['url'])
                    imgui.VoidText(sound['url'].v, '������� ������ URL ������ �� ���� (.mp3)')
                    imgui.SameLine(405); imgui.TestSound(sound['url'].v)
        
                    imgui.PopItemWidth()
        
                    if imgui.Button(u8'��������� � �������', imgui.ImVec2(468, 20)) then 
                        imgui.CloseCurrentPopup()
                    end
        
                    imgui.EndPopup()
                end
            end
            imgui.PushItemWidth(100)
            if imgui.InputInt('##delayGov', delayGov) then 
                if delayGov.v < 0 then delayGov.v = 0 end
                if delayGov.v > 10000 then delayGov.v = 10000 end
            end
            imgui.PopItemWidth()
            imgui.SameLine()
            imgui.TextColoredRGB(u8('��������')) -- {565656}(?)')
            imgui.Question(u8'�������� ����� ����������� � GOV-�����\n����������� � ������������� (1000 �� = 1 ���)')
            imgui.SetCursorPos(imgui.ImVec2(300, 10))
            if not status_button_gov then 
                if tonumber(os.date("%H", os.time())) == hGov.v and tonumber(os.date("%M", os.time())) == mGov.v then
                    imgui.DisableButton(u8'��������##gov', imgui.ImVec2(-1, -1))
                else
                    if imgui.GreenButton(u8'��������##gov', imgui.ImVec2(-1, -1)) then 
                        status_button_gov = true
                        antiflud = true
                    end
                end
            else
                if imgui.RedButton(u8'�� ������: '..getOstTime(), imgui.ImVec2(-1, -1)) then 
                    status_button_gov = false
                end
            end
            imgui.EndChild()
        imgui.End()
    end
end
function rpNick(id)
	local nick = sampGetPlayerNickname(id)
	if nick:match('_') then return nick:gsub('_', ' ') end
	return nick
end
function updateDate()
    local strDate = thisScript().description
    local d, m, y = strDate:match('(%d+)%.(%d+)%.(%d+)')
    local tDateUpdate = { 
        year = y,
        month = m,
        day = d
    }
    return getStrDate(os.time(tDateUpdate))
end
function getStrDate(unixTime)
    local tMonths = {'������', '�������', '�����', '������', '���', '����', '����', '�������', '��������', '�������', '������', '�������'}
    local day = tonumber(os.date('%d', unixTime))
    local month = tMonths[tonumber(os.date('%m', unixTime))]
    local year = tonumber(os.date('%Y', unixTime))
    return string.format('%s %s %s', day, month, year)
end
function daysAfterUpdate()
    local strDate = thisScript().description
    local d, m, y = strDate:match('(%d+)%.(%d+)%.(%d+)')
    local tDateUpdate = { 
        year = y,
        month = m,
        day = d
    }
    local tDateNow = {
        year = os.date("%Y", os.time()),
        month = os.date("%m", os.time()),
        day = os.date("%d", os.time())
    }
    return math.floor((os.time(tDateNow) - os.time(tDateUpdate)) / 86400)
end

function isVersionCurrent(strVer)
    local cur = scriptversion:match('^(%d+%.%d+).+')
    local new = strVer:match('^(%d+%.%d+).+')
    return cur == new
end

function sampev.onSendCommand(cmd)
    if cmd:match('/giverank %d+ %d+') then
    	local id, rank = cmd:match('/giverank (%d+) (%d+)')
    	local rank = tonumber(rank)
    	if sampIsPlayerConnected(id) and rank > 1 and rank < 10 then
    		tOutput = {
				['!member'] 		= sampGetPlayerNickname(id),
				['!rankNameOld'] 	= u8:decode(ImRank[rank - 1].v),
				['!rankNameNew']	= u8:decode(ImRank[rank].v),
				['!rankOld'] 		= rank - 1,
				['!rankNew'] 		= rank,
				['!date'] 			= tInfo['date'],
				['!time'] 			= tInfo['time'],
				['!myName'] 		= tInfo['selfName'],
				['!myId'] 			= tInfo['selfId']
			}
    		writeLog('/giverank')
    	end
    end
end
function writeLog(nameLog)
	local Log = io.open(mainPath..'\\'..nameLog:gsub('/', '')..'.txt', "a")
	Log:write(getExitString(nameLog, false)..'\n')
	Log:close()
end
function getExitString(lable, demo)
	local demo = demo and true or false
	local template = u8:decode(ImEdit[lable].v)
	local tDemo = {
		['!member'] 		= 'Nick_Name',
		['!rankNameOld'] 	= '����� ��',
		['!rankNameNew']	= '��. ����� ��',
		['!rankOld'] 		= '3',
		['!rankNew'] 		= '4',
		['!date'] 			= '01.01.2020',
		['!time'] 			= '10:50:25',
		['!myName'] 		= tInfo['selfName'],
		['!myId'] 			= tInfo['selfId'],
	}

	local isPatternAllowed = function(typeLog, pattern)
		tAllowedPatterns = {
			['/giverank'] 	= {'!member', '!rankNameOld', '!rankNameNew', '!rankOld', '!rankNew', '!date', '!time', '!myId', '!myName'},
		}
		for k, v in pairs(tAllowedPatterns[typeLog]) do 
			if v == pattern then return true end
		end
		return false
	end

	local getResultFromTemplate = function(typeLog, template, pattern)
		local bPat = isPatternAllowed(typeLog, pattern)
		local errPat = '{FF2000}'..pattern..'{SSSSSS}'
		return template:gsub(pattern, (bPat and (demo and tDemo[pattern] or tOutput[pattern]) or errPat))
	end

	local template = getResultFromTemplate(lable, template, '!member')
	local template = getResultFromTemplate(lable, template, '!rankNameOld')
	local template = getResultFromTemplate(lable, template, '!rankNameNew')
	local template = getResultFromTemplate(lable, template, '!rankOld')
	local template = getResultFromTemplate(lable, template, '!rankNew')
	local template = getResultFromTemplate(lable, template, '!date')
	local template = getResultFromTemplate(lable, template, '!time')
	local template = getResultFromTemplate(lable, template, '!myName')
	local template = getResultFromTemplate(lable, template, '!myId')
	return template
end
function showPattern(patName, patInfo)
	if imgui.Button(u8(patName), imgui.ImVec2(230, 30)) then 
		setClipboardText(patName)
        ImEdit['/giverank'].v = ImEdit['/giverank'].v..patName
	end
	imgui.Hint(u8(patInfo))
end
function imgui.TestSound(var)
    if var:match('^http%a*://.+%.mp3$') then
        if var == configuration.sound.url then 
            if imgui.Button('Play', imgui.ImVec2(71, 20)) then
                setAudioStreamState(soundurl, as.PLAY) 
            end
        else
            if imgui.Button('Load', imgui.ImVec2(71, 20)) then
                soundurl = loadAudioStream(tostring(var))
                if soundurl then
                    configuration.sound.url = sound['url'].v
                    setAudioStreamState(soundurl, as.PLAY)
                else
                    sampAddChatMessage(tag..'������ ����� �� ���������� ���� ������ �� ����������!', mcx) 
                    sound['url'].v = ''
                end
            end
        end
    else
        imgui.DisableButton('Play', imgui.ImVec2(70, 20))
        imgui.Hint(u8('������� ������� ������ �� ����, ��� ����� �����������\n������ ������ ��������� � ��������� �������:\nhttps://www.myinstants.com/media/sounds/swallow-my-cum_RpUPJTV.mp3'))
    end
end
function imgui.VoidText(bufvar, text)
    if #bufvar == 0 then
        imgui.SameLine(15)
        imgui.TextColored(imgui.ImVec4(0.36, 0.42, 0.47, 1.00), u8(text))
    end
end
function sampev.onServerMessage(clr, msg)
    if msg:find('{screen}') then
        local screenMsg = msg:gsub('{screen}', '')
        if #screenMsg > 0 then sampSendChat(screenMsg) end
        lua_thread.create(function()
	        wait(300)
			sampSendChat('/time')
			wait(500)
			setVirtualKeyDown(VK_F8, true)
			wait(0)
			setVirtualKeyDown(VK_F8, false)
		end)
        return false
    end
    if getMembers then 
		if msg:find('%[������%] {FFFFFF}�� �� �������� �� �������') then 
			return false
		end
	end
end
function sampev.onPlaySound(sound, pos) -- ������������ ����� ��� �������� ��������
	if getMembers and sound == 1052 then 
		return false
	end
end
function checkMembers()
    wait(5000)
    if not sampIsDialogActive() and sampIsLocalPlayerSpawned() then
        getMembers = true
        sampSendChat('/members')
    end
    return true
end
function sampev.onShowDialog(dialogId, style, title, button1, button2, text)
	if dialogId == 2015 and getMembers then
		countt = 0
        online = title:match('.+%(� ����: (%d+)%)')
        for line in text:gmatch('[^\r\n]+') do
            if not line:find('��� ������') and not line:find('��������� ��������') and not line:find('���������� ��������') then
                local tid, trank = line:match('.+%((%d+)%)\t.+%((%d+)%)\t%d\t%d+')
                table.insert(members, {tid, trank})
            end

            if text:find('��������� ��������') then
                if not line:find('��� ������') and not line:find('���������� ��������') then
                    if line:find('��������� ��������') then
                        sampSendDialogResponse(dialogId, 1, countt, nil)
                    end
                    countt = countt + 1
                end
            else 
                getMembers = false
                sampSendDialogResponse(dialogId, 0, -1, nil)
                while #members > tonumber(online) do table.remove(members, 1) end
            end
        end
    	return false
	end
end
function downloadFile(name, path, link)
	if not doesFileExist(path) then
		log('���������� ����� '..mc..'�'..name..'�')
		downloadUrlToFile(link, path, function(id, status, p1, p2)
			if status == dlstatus.STATUSEX_ENDDOWNLOAD then
				if doesFileExist(path) then
					log('���� '..mc..'�'..name..'�'..wc..' ��������!')
				else
					log('�� ������� ��������� ���� '..mc..'�'..name..'�')
				end
			end
		end)
	end
end
function log(text)
	sampfuncsLog(mc..'GOV-Helper: '..wc..text)
end
function loadlogs()
    createDirectory(mainPath)
    for nameLog, _ in pairs(configuration.logs) do
        local nameLog = nameLog:gsub('/', '')..'.txt'
        local pathLog = mainPath..'\\'..nameLog
        if not doesFileExist(pathLog) then 
            local newLog = io.open(pathLog, "a"); newLog:close()
            log('������ ���� '..mc..nameLog)
        end
    end
end
function checkData()
	log('�������� ������� ������ ��� ������ �������')
	local bLibs = checklibs()
	if bLibs then
		createDirectory(getWorkingDirectory()..'\\GOVh')
		return true
	end
	return false
end

function checklibs()
	if not bFa or not bNotf then
		lua_thread.create(function()
			log('��������� ����������� ���������..')
			createDirectory(getWorkingDirectory()..'\\GOVh')
			downloadFile('fa-solid-900', getWorkingDirectory()..'\\GOVh\\fa-solid-900.ttf', 'https://gitlab.com/Ratatata/govhelper/-/raw/master/fa-solid-900.ttf?inline=false')
		 	while not doesFileExist(getWorkingDirectory()..'\\GOVh\\fa-solid-900.ttf') do wait(0) end
		 	downloadFile('fAwesome5', getWorkingDirectory()..'\\lib\\fAwesome5.lua', 'https://gitlab.com/Ratatata/govhelper/-/raw/master/fAwesome5.lua')
			while not doesFileExist(getWorkingDirectory()..'\\lib\\fAwesome5.lua') do wait(0) end
			downloadFile('Notify', getWorkingDirectory()..'\\GOVh\\notify.lua', 'https://gitlab.com/Ratatata/govhelper/-/raw/master/notify.lua')
			while not doesFileExist(getWorkingDirectory()..'\\GOVh\\notify.lua') do wait(0) end			
			log('��������� ����������� ��������� ��������. ��������������..')
			wait(1000)
			thisScript():reload() 
		end)
		return false
	end
	return true
end
function goGov()
	lua_thread.create(function()
		if #govdep[1].v > 0 then sampSendChat('/d '..u8:decode(govdep[1].v)) end
	    if #govstr[1].v > 0 then wait(delayGov.v); sampSendChat('/gov '..u8:decode(govstr[1].v)) end
	    if #govstr[2].v > 0 then wait(delayGov.v); sampSendChat('/gov '..u8:decode(govstr[2].v)) end
	    if #govstr[3].v > 0 then wait(delayGov.v); sampSendChat('/gov '..u8:decode(govstr[3].v)) end
	    if #govdep[1].v > 0 then wait(delayGov.v); sampSendChat('/d '..u8:decode(govdep[2].v)) end
		if gosScreen.v then wait(500); sampSendChat('{screen}') end
	end)
end
function onWindowMessage(msg, wparam, lparam)
    if msg == 0x0100 and wparam == 0x1B then
        if IW_main.v then consumeWindowMessage() IW_main.v = false
        elseif IW_log.v then consumeWindowMessage() IW_log.v = false
        elseif IW_vz.v then consumeWindowMessage() IW_vz.v = false 
        elseif IW_changelog.v then
            configuration.main.clogshow = false
            inicfg.save(configuration, 'GOVconfig.ini')
            consumeWindowMessage() 
            IW_changelog.v = false 
        end
    end
end
function get_timer(time)
	return string.format("%s:%s:%s", string.format("%s%s", (tonumber(os.date("%H", time)) < tonumber(os.date("%H", 0)) and 24 + tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0)) or tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0))) < 10 and 0 or "", tonumber(os.date("%H", time)) < tonumber(os.date("%H", 0)) and 24 + tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0)) or tonumber(os.date("%H", time)) - tonumber(os.date("%H", 0))), os.date("%M", time), os.date("%S", time))
end
function imgui.CenterTextColoredRGB(text)
    local width = imgui.GetWindowWidth()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local textsize = w:gsub('{.-}', '')
            local text_width = imgui.CalcTextSize(u8(textsize))
            imgui.SetCursorPosX( width / 2 - text_width .x / 2 )
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else
                imgui.Text(u8(w))
            end
        end
    end
    render_text(text)
end

function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], text[i])
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(w) end
        end
    end

    render_text(text)
end
function imgui.Question(...)
    imgui.SameLine()
    imgui.TextDisabled(fa.ICON_FA_QUESTION_CIRCLE)
    imgui.Hint(...)
end
function getOstTime()
	local datetime = {
		year  = os.date("%Y", os.time()),
	    month = os.date("%m", os.time()),
	    day   = os.date("%d", os.time()),
	    hour  = hGov.v,
	    min   = mGov.v,
	    sec   = 00
   	}
   	if os.time(datetime) - os.time() < 0 then
   		datetime.day = os.date("%d", os.time()) + 1
   		return tostring(get_timer(os.time(datetime) - os.time()))
   	else
   		return tostring(get_timer(os.time(datetime) - os.time()))
   	end
end
function imgui.RedButton(text, size)
	imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.50, 0.00, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.40, 0.00, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.30, 0.00, 0.00, 1.00))
 		local button = imgui.Button(text, size)
	imgui.PopStyleColor(3)
	return button
end

function imgui.GreenButton(text, size)
	imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.00, 0.50, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.00, 0.40, 0.00, 1.00))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.00, 0.30, 0.00, 1.00))
 		local button = imgui.Button(text, size)
	imgui.PopStyleColor(3)
	return button
end

function imgui.DisableButton(text, size)
	imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(0.0, 0.0, 0.0, 0.2))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(0.0, 0.0, 0.0, 0.2))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(0.0, 0.0, 0.0, 0.2))
 		local button = imgui.Button(text, size)
	imgui.PopStyleColor(3)
	return button
end
function imgui.Hint(text, delay, action)
    if imgui.IsItemHovered() then
        if go_hint == nil then go_hint = os.clock() + (delay and delay or 0.0) end
        local alpha = (os.clock() - go_hint) * 5
        if os.clock() >= go_hint then 
		    imgui.PushStyleVar(imgui.StyleVar.WindowPadding, imgui.ImVec2(10, 10))
		    imgui.PushStyleVar(imgui.StyleVar.Alpha, (alpha <= 1.0 and alpha or 1.0))
		        imgui.PushStyleColor(imgui.Col.PopupBg, imgui.GetStyle().Colors[imgui.Col.WindowBg])
		            imgui.BeginTooltip()
		            imgui.PushTextWrapPos(450)
		            imgui.TextColored(imgui.GetStyle().Colors[imgui.Col.ButtonHovered], fa.ICON_FA_INFO_CIRCLE..u8' ���������:')
		            imgui.TextUnformatted(text)
		            if action ~= nil then 
		            	imgui.TextColored(imgui.GetStyle().Colors[imgui.Col.TextDisabled], '\n'..fa.ICON_FA_SHARE..' '..action)
		            end
		            if not imgui.IsItemVisible() and imgui.GetStyle().Alpha == 1.0 then go_hint = nil end
		            imgui.PopTextWrapPos()
		            imgui.EndTooltip()
		        imgui.PopStyleColor()
		    imgui.PopStyleVar(2)
		end
    end
end
function imgui.ToggleButton(str_id, bool)
    local rBool = false

	if LastActiveTime == nil then
		LastActiveTime = {}
	end
	if LastActive == nil then
		LastActive = {}
	end

	local function ImSaturate(f)
		return f < 0.0 and 0.0 or (f > 1.0 and 1.0 or f)
	end
	
	local p = imgui.GetCursorScreenPos()
	local draw_list = imgui.GetWindowDrawList()

	local height = imgui.GetTextLineHeightWithSpacing()
	local width = height * 1.55
	local radius = height * 0.50
	local ANIM_SPEED = 0.15

	if imgui.InvisibleButton(str_id, imgui.ImVec2(width, height)) then
		bool.v = not bool.v
		rBool = true
		LastActiveTime[tostring(str_id)] = os.clock()
		LastActive[tostring(str_id)] = true
	end

	local t = bool.v and 1.0 or 0.0

	if LastActive[tostring(str_id)] then
		local time = os.clock() - LastActiveTime[tostring(str_id)]
		if time <= ANIM_SPEED then
			local t_anim = ImSaturate(time / ANIM_SPEED)
			t = bool.v and t_anim or 1.0 - t_anim
		else
			LastActive[tostring(str_id)] = false
		end
	end

	local col_bg
	if bool.v then
		col_bg = imgui.GetColorU32(imgui.GetStyle().Colors[imgui.Col.FrameBgHovered])
	else
		col_bg = imgui.ImColor(100, 100, 100, 180):GetU32()
	end

	draw_list:AddRectFilled(imgui.ImVec2(p.x, p.y + (height / 6)), imgui.ImVec2(p.x + width - 1.0, p.y + (height - (height / 6))), col_bg, 5.0)
	draw_list:AddCircleFilled(imgui.ImVec2(p.x + radius + t * (width - radius * 2.0), p.y + radius), radius - 0.75, imgui.GetColorU32(bool.v and imgui.GetStyle().Colors[imgui.Col.ButtonActive] or imgui.ImColor(150, 150, 150, 255):GetVec4()))

	return rBool
end
function timerupdate()
    while aupd do
        checkupdate(jsn_upd, tag, url_upd)
        wait(300000)
        return true
    end
end
function checkupdate(json_url, prefix, url)
    local dlstatus = require('moonloader').download_status
    local json = getWorkingDirectory() .. '\\'..thisScript().name..'-version.json'
    if doesFileExist(json) then os.remove(json) end
    log('������ �������� ����������')
    downloadUrlToFile(json_url, json,
        function(id, status, p1, p2)
            if status == dlstatus.STATUSEX_ENDDOWNLOAD then
                if doesFileExist(json) then
                    log('���������� JSON ������� ��������')
                    local f = io.open(json, 'r')
                    if f then
                        local info = decodeJson(f:read('*a'))
                        updatelink = info.updateurl
                        updateversion = info.latest
                        f:close()
                        os.remove(json)
                        log('������� ���������, ������ ������ ������')
                        if updateversion ~= thisScript().version then
                            lua_thread.create(function(prefix)
                                local dlstatus = require('moonloader').download_status
                                log('������� ���������� '..thisScript().version..' -> '..updateversion..'!')
                                sampAddChatMessage(tag..'������� ���������� '..thisScript().version..' -> '..updateversion..'!',mcx)
                                sampAddChatMessage(tag..'�������� �������� ����� 5 ������!', mcx)
                                wait(5000)
                                downloadUrlToFile(updatelink, thisScript().path,
                                function(id3, status1, p13, p23)
                                    if status1 == dlstatus.STATUS_DOWNLOADINGDATA then
                                        log('���������� ���������� ������ �������')
                                    elseif status1 == dlstatus.STATUS_ENDDOWNLOADDATA then
                                        log('���������� ��������. ������ �������� �� ������ '..mc..updateversion)
                                        goupdatestatus = true
                                        configuration.main.clogshow = true
                                        inicfg.save(configuration, 'GOVconfig.ini')
                                        lua_thread.create(function()
                                            log('������������ �������')
                                            wait(500)
                                            thisScript():reload() 
                                        end)
                                    end
                                    if status1 == dlstatus.STATUSEX_ENDDOWNLOAD then
                                        if goupdatestatus == nil then
                                            log('������ �� ���� ��������� �� ������ '..updateversion)
                                            update = false
                                        end
                                    end
                                end)	
                            end, prefix)
                        else
                            log('������ ���������. ���������� ���')
                        end
                    end
                else
                    log('�� ������� �������� JSON �������', 0xFF0000)
                end
            end
        end)
end
function style_gray()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
    style.WindowRounding = 5.0
    style.FrameRounding = 5.0
    style.ChildWindowRounding = 5.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    style.GrabMinSize = 8
    style.GrabRounding = 2
  

	colors[clr.Text] 					= ImVec4(0.95, 0.96, 0.98, 1.00)
    colors[clr.TextDisabled] 			= ImVec4(0.36, 0.42, 0.47, 1.00)
	colors[clr.WindowBg] 				= ImVec4(0.11, 0.15, 0.17, 1.00)
	colors[clr.ChildWindowBg] 			= ImVec4(0.15, 0.18, 0.22, 1.00)
	colors[clr.PopupBg] 				= ImVec4(0.15, 0.18, 0.22, 1.00)
	colors[clr.Border] 					= ImVec4(0.86, 0.86, 0.86, 0.50)
	colors[clr.BorderShadow] 			= ImVec4(0.00, 0.00, 0.00, 0.00)
	colors[clr.FrameBg] 				= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.FrameBgHovered] 			= ImVec4(0.12, 0.20, 0.28, 1.00)
	colors[clr.FrameBgActive] 			= ImVec4(0.09, 0.12, 0.14, 1.00)
    colors[clr.TitleBg] 				= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.TitleBgCollapsed] 		= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.TitleBgActive] 			= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.MenuBarBg] 				= ImVec4(0.15, 0.18, 0.22, 1.00)
	colors[clr.ScrollbarBg] 			= ImVec4(0.02, 0.02, 0.02, 0.39)
	colors[clr.ScrollbarGrab] 			= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.ScrollbarGrabHovered] 	= ImVec4(0.18, 0.22, 0.25, 1.00)
	colors[clr.ScrollbarGrabActive] 	= ImVec4(0.09, 0.21, 0.31, 1.00)
	colors[clr.ComboBg] 				= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.CheckMark] 				= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.SliderGrab] 				= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.SliderGrabActive] 		= ImVec4(0.37, 0.61, 1.00, 1.00)
	colors[clr.Button] 					= ImVec4(0.20, 0.25, 0.29, 1.00)
	colors[clr.ButtonHovered] 			= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.ButtonActive]			= ImVec4(0.06, 0.53, 0.98, 1.00)
	colors[clr.Header] 					= ImVec4(0.20, 0.25, 0.29, 0.55)
	colors[clr.HeaderHovered] 			= ImVec4(0.26, 0.59, 0.98, 0.80)
	colors[clr.HeaderActive] 			= ImVec4(0.26, 0.59, 0.98, 1.00)
	colors[clr.ResizeGrip] 				= ImVec4(0.26, 0.59, 0.98, 0.25)
	colors[clr.ResizeGripHovered] 		= ImVec4(0.26, 0.59, 0.98, 0.67)
	colors[clr.ResizeGripActive] 		= ImVec4(0.06, 0.05, 0.07, 1.00)
    colors[clr.CloseButton]             = ImVec4(1.00, 0.00, 0.00, 0.50)
    colors[clr.CloseButtonHovered]      = ImVec4(1.00, 0.00, 0.00, 0.80)
    colors[clr.CloseButtonActive]       = ImVec4(1.00, 0.00, 0.00, 1.00)
	colors[clr.PlotLines] 				= ImVec4(0.61, 0.61, 0.61, 1.00)
	colors[clr.PlotLinesHovered] 		= ImVec4(1.00, 0.43, 0.35, 1.00)
	colors[clr.PlotHistogram] 			= ImVec4(0.90, 0.70, 0.00, 1.00)
	colors[clr.PlotHistogramHovered] 	= ImVec4(1.00, 0.60, 0.00, 1.00)
	colors[clr.TextSelectedBg] 			= ImVec4(0.28, 0.56, 1.00, 1.00)
	colors[clr.ModalWindowDarkening] 	= ImVec4(1.00, 1.00, 1.00, 0.30)

	mc, sc, wc = '{006AC2}', '{006D86}', '{FFFFFF}'
	mcx = 0x006AC2
end
style_gray()